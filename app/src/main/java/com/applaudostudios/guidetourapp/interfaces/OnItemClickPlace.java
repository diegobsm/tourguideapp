package com.applaudostudios.guidetourapp.interfaces;

import com.applaudostudios.guidetourapp.model.Place;

/**
 * This interface is used as callback when the user makes click in one item
 */
public interface OnItemClickPlace {
    /**
     * this function take one parameter of type Place
     *
     * @param place of item click
     */
    void onClick(Place place);
}
