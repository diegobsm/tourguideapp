package com.applaudostudios.guidetourapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.applaudostudios.guidetourapp.R;
import com.applaudostudios.guidetourapp.interfaces.OnItemClickPlace;
import com.applaudostudios.guidetourapp.model.Place;

import java.util.ArrayList;
import java.util.List;

public class CategoryPlaceAdapter extends RecyclerView.Adapter<CategoryPlaceAdapter.CategoryPlaceAdapterViewHolder> implements Filterable {

    private List<Place> mPlaces;
    private List<Place> mFilterPlace;
    private OnItemClickPlace mListener;

    /**
     * init the lists and closes the contract for callback
     *
     * @param onItemClickPlace of type OnItemClickPlace
     */
    public CategoryPlaceAdapter(OnItemClickPlace onItemClickPlace) {
        mPlaces = new ArrayList<>();
        mListener = onItemClickPlace;
        mFilterPlace = new ArrayList<>();
    }

    /**
     * This function receives one parameter of type list places and call to method notifyDataSetChanged()
     * for update tha data of the recyclerView
     *
     * @param pl of type list of places
     */
    public void setmPlaces(List<Place> pl) {
        mPlaces = pl;
        mFilterPlace = mPlaces;
        notifyDataSetChanged();
    }

    /**
     *
     */
    public void restoreListByFilterClose() {
        mPlaces = mFilterPlace;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CategoryPlaceAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryPlaceAdapterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_places, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryPlaceAdapterViewHolder holder, int position) {
        holder.bindView(mPlaces.get(position));
    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    mPlaces = mFilterPlace;
                } else {
                    List<Place> filteredList = new ArrayList<>();
                    for (Place row : mFilterPlace) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getmNamePlace().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    mPlaces = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mPlaces;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mPlaces = (List<Place>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class CategoryPlaceAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imvPlace;
        private TextView txvPlace;
        private CardView cardView;
        private View view;

        public CategoryPlaceAdapterViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            imvPlace = itemView.findViewById(R.id.imvPlace);
            txvPlace = itemView.findViewById(R.id.txvPlace);
            cardView = itemView.findViewById(R.id.crvMain);
            cardView.setOnClickListener(this);
        }

        /**
         * this function receives one parameter of type place and assigns the data
         * to textView and to ImageView
         *
         * @param place of Type place that was selected
         */
        private void bindView(Place place) {
            imvPlace.setImageDrawable(view.getContext().getResources().getDrawable(place.getmImages()));
            txvPlace.setText(place.getmNamePlace());
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(mPlaces.get(getAdapterPosition()));
        }
    }
}
