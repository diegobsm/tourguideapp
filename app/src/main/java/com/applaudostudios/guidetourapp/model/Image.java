package com.applaudostudios.guidetourapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.applaudostudios.guidetourapp.R;

import java.util.ArrayList;
import java.util.List;

public class Image implements Parcelable {

    private Integer mPath;

    private Integer getPath() {
        return mPath;
    }

    public void setPath(Integer path) {
        this.mPath = path;
    }

    /**
     * this function create an array of int with all the resources of images
     * and return this array
     *
     * @param idCategory for makes the condition
     * @return array the ints
     */
    public static int[] getImagesByPlace(int idCategory) {

        switch (idCategory) {
            case Place.CATEGORY_VOLCANO:
                return new int[]{
                        R.drawable.volcano_ilamatepec,
                        R.drawable.volcano_izalco,
                        R.drawable.volcano_san_salvador,
                        R.drawable.volcano_chaparrastique,
                        R.drawable.volcano_chinchotepec,
                        R.drawable.volcano_conchagua

                };
            case Place.CATEGORY_FOREST:
                return new int[]{
                        R.drawable.bicentent_park,
                        R.drawable.cerro_verde,
                        R.drawable.imposible,
                        R.drawable.balboa,
                        R.drawable.montecristo,
                        R.drawable.pital
                };
            case Place.CATEGORY_BEACH:
                return new int[]{
                        R.drawable.costa_de_el_sol,
                        R.drawable.las_flores,
                        R.drawable.el_tunco,
                        R.drawable.el_sunzal,
                        R.drawable.el_zonte,
                        R.drawable.los_cobanos
                };
            case Place.CATEGORY_AQUATIC_PARK:
                return new int[]{
                        R.drawable.los_chorros,
                        R.drawable.atlantis,
                        R.drawable.amapulapa,
                        R.drawable.apuzunga,
                        R.drawable.los_chorros_de_la_calera,
                        R.drawable.ichanmiche
                };
            case Place.CATEGORY_LAKE:
                return new int[]{
                        R.drawable.lake_suchitlan,
                        R.drawable.lake_ilopango,
                        R.drawable.lake_of_coatepeque,
                        R.drawable.lagoon_of_apastepeque,
                        R.drawable.lagoon_alegria,
                        R.drawable.apaneca
                };
            default:
                return new int[0];
        }

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mPath);
    }

    public Image() {
    }

    protected Image(Parcel in) {
        this.mPath = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
