package com.applaudostudios.guidetourapp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Category implements Parcelable {
    private String mName;
    private List<Place> mPlaces;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public List<Place> getmPlaces() {
        return mPlaces;
    }

    public void setmPlaces(List<Place> mPlaces) {
        this.mPlaces = mPlaces;
    }

    /**
     * This function creates a object of type Category with name and the list the Places
     * of that category
     *
     * @param nameCategory name of category
     * @param idCategory   for identify which category selection
     * @param context      for access to resources
     * @return a object Category with name and the places from that category
     */
    public static Category getCategory(String nameCategory, int idCategory, Context context) {
        return new Category(nameCategory, Place.selectPlacesByCategory(idCategory, context));
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mName);
        dest.writeTypedList(this.mPlaces);
    }

    public Category() {
    }

    public Category(String nameCategory, List<Place> places) {
        mName = nameCategory;
        mPlaces = places;
    }

    protected Category(Parcel in) {
        this.mName = in.readString();
        this.mPlaces = in.createTypedArrayList(Place.CREATOR);
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
