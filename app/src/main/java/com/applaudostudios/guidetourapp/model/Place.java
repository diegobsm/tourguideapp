package com.applaudostudios.guidetourapp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.applaudostudios.guidetourapp.R;

import java.util.ArrayList;
import java.util.List;

public class Place implements Parcelable {

    public static final int CATEGORY_VOLCANO = 0;
    public static final int CATEGORY_FOREST = 1;
    public static final int CATEGORY_BEACH = 2;
    public static final int CATEGORY_AQUATIC_PARK = 3;
    public static final int CATEGORY_LAKE = 4;

    private String mNamePlace;
    private String mDescription;
    private String mAddress;
    private double mLatitude;
    private double mLongitude;
    private String mWebSide;
    private String mPhone;
    private int mImages;

    public String getmNamePlace() {
        return mNamePlace;
    }

    public void setmNamePlace(String mNamePlace) {
        this.mNamePlace = mNamePlace;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmWebSite() {
        return mWebSide;
    }

    public void setmWebSide(String mWebSide) {
        this.mWebSide = mWebSide;
    }

    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public int getmImages() {
        return mImages;
    }

    public void setmImages(int mImages) {
        this.mImages = mImages;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    /**
     * This function takes two parameters and depending of category gets string-array with tha places
     * and return this list of places
     *
     * @param idCategory for make the condition
     * @param context    for access to resources
     * @return list of places by category
     */
    public static List<Place> selectPlacesByCategory(int idCategory, Context context) {
        switch (idCategory) {
            case CATEGORY_VOLCANO:
                return getListPlacesByCategory(context.getResources().getStringArray(R.array.description_volcano_1), context.getResources().getStringArray(R.array.description_volcano_2),
                        context.getResources().getStringArray(R.array.description_volcano_3), context.getResources().getStringArray(R.array.description_volcano_4),
                        context.getResources().getStringArray(R.array.description_volcano_5), context.getResources().getStringArray(R.array.description_volcano_6),
                        Image.getImagesByPlace(idCategory));
            case CATEGORY_FOREST:
                return getListPlacesByCategory(context.getResources().getStringArray(R.array.description_forest_1), context.getResources().getStringArray(R.array.description_forest_2),
                        context.getResources().getStringArray(R.array.description_forest_3), context.getResources().getStringArray(R.array.description_forest_4),
                        context.getResources().getStringArray(R.array.description_forest_5), context.getResources().getStringArray(R.array.description_forest_6),
                        Image.getImagesByPlace(idCategory));
            case CATEGORY_BEACH:
                return getListPlacesByCategory(context.getResources().getStringArray(R.array.description_beach_1), context.getResources().getStringArray(R.array.description_beach_2),
                        context.getResources().getStringArray(R.array.description_beach_3), context.getResources().getStringArray(R.array.description_beach_4),
                        context.getResources().getStringArray(R.array.description_beach_5), context.getResources().getStringArray(R.array.description_beach_6),
                        Image.getImagesByPlace(idCategory));
            case CATEGORY_AQUATIC_PARK:
                return getListPlacesByCategory(context.getResources().getStringArray(R.array.description_aquatic_park_1), context.getResources().getStringArray(R.array.description_aquatic_park_2),
                        context.getResources().getStringArray(R.array.description_aquatic_park_3), context.getResources().getStringArray(R.array.description_aquatic_park_4),
                        context.getResources().getStringArray(R.array.description_aquatic_park_5), context.getResources().getStringArray(R.array.description_aquatic_park_6),
                        Image.getImagesByPlace(idCategory));

            case CATEGORY_LAKE:
                return getListPlacesByCategory(context.getResources().getStringArray(R.array.description_lake_1), context.getResources().getStringArray(R.array.description_lake_2),
                        context.getResources().getStringArray(R.array.description_lake_3), context.getResources().getStringArray(R.array.description_lake_4),
                        context.getResources().getStringArray(R.array.description_lake_5), context.getResources().getStringArray(R.array.description_lake_6),
                        Image.getImagesByPlace(idCategory));
            default:
                return new ArrayList<>();
        }
    }


    /**
     * this function takes seven parameters and creates a list of object Places
     *
     * @param place1 array of string place one
     * @param place2 array of string place two
     * @param place3 array of string place three
     * @param place4 array of string place four
     * @param place5 array of string place five
     * @param place6 array of string place six
     * @param path   array of Integers that contains the ids of resources the images
     * @return
     */
    private static List<Place> getListPlacesByCategory(String[] place1, String[] place2, String[] place3, String[] place4, String[] place5, String[] place6, int[] path) {
        List<Place> places = new ArrayList<>();
        places.add(getPlaceAndDescription(place1, path[0]));
        places.add(getPlaceAndDescription(place2, path[1]));
        places.add(getPlaceAndDescription(place3, path[2]));
        places.add(getPlaceAndDescription(place4, path[3]));
        places.add(getPlaceAndDescription(place5, path[4]));
        places.add(getPlaceAndDescription(place6, path[5]));
        return places;
    }

    /**
     * This function takes two parameters and creates an Object Places
     * that is fills with the data of the array of string and the path of the resource
     *
     * @param placeString array of string the one places
     * @param path        int with the id of the resource
     * @return a object Place
     */
    private static Place getPlaceAndDescription(String[] placeString, int path) {
        Place place = new Place();
        place.mNamePlace = placeString[0];
        place.mDescription = placeString[1];
        place.mAddress = placeString[2];
        place.mPhone = placeString[3];
        place.mLatitude = Double.parseDouble(placeString[4]);
        place.mLongitude = Double.parseDouble(placeString[5]);
        place.mWebSide = placeString[6];
        place.setmImages(path);

        return place;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mNamePlace);
        dest.writeString(this.mDescription);
        dest.writeString(this.mAddress);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeString(this.mWebSide);
        dest.writeString(this.mPhone);
        dest.writeInt(this.mImages);
    }

    public Place() {
    }

    protected Place(Parcel in) {
        this.mNamePlace = in.readString();
        this.mDescription = in.readString();
        this.mAddress = in.readString();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mWebSide = in.readString();
        this.mPhone = in.readString();
        this.mImages = in.readInt();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
