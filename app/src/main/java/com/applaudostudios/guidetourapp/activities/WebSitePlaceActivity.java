package com.applaudostudios.guidetourapp.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.applaudostudios.guidetourapp.R;

public class WebSitePlaceActivity extends AppCompatActivity {

    public static final String EXTRA_WEBSITE = "EXTRA_WEBSITE";

    /**
     * This function creates a intent with data extra en returns this intent
     *
     * @param context context for make intent
     * @param ulr     for abrir in webView
     * @return intent explicit
     */
    public static Intent getIntent(Context context, String ulr) {
        return new Intent(context, WebSitePlaceActivity.class).putExtra(EXTRA_WEBSITE, ulr);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_site_place);
        setActionBar(((Toolbar) findViewById(R.id.tbWv)));
        String webSite = getIntent().getStringExtra(EXTRA_WEBSITE);
        WebView webView = findViewById(R.id.wvPlace);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(webSite);
    }

    /**
     * This function take one parameter of type ToolBar and is assigns to action bar
     * and actives the icon backHome
     *
     * @param toolbar for assigns to actionBar
     */
    private void setActionBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_keyboard_backspace_white_24dp));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
