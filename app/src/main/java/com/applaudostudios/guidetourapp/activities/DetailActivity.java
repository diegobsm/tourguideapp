package com.applaudostudios.guidetourapp.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applaudostudios.guidetourapp.R;
import com.applaudostudios.guidetourapp.model.Place;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DETAIL_PLACE = "DETAIL_PLACE";
    private static final int ACTION_VIEW_PHONE = 1;
    private static final int ACTION_VIEW_MAP = 2;
    private ImageView mImageView;
    private TextView mDescription;
    private TextView mPhone;
    private TextView mAddress;
    private TextView mWebSite;
    private Place mPlace;


    /**
     * This function creates a intent with data extra en returns this intent
     *
     * @param context context for make intent
     * @param place   for fill tha vies of the detail
     * @return intent explicit
     */
    public static Intent getInstance(Context context, Place place) {
        return new Intent(context, DetailActivity.class).putExtra(DETAIL_PLACE, place);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initView();
    }

    /**
     * This function initializes all the views
     */
    private void initView() {
        setActionBar(((Toolbar) findViewById(R.id.tbDetail)));
        mImageView = findViewById(R.id.imvPlaceDetail);
        mDescription = findViewById(R.id.txvDescription);
        mPhone = findViewById(R.id.txvPhone);
        mAddress = findViewById(R.id.txvAddress);
        mWebSite = findViewById(R.id.txvWebSite);
        mPhone.setOnClickListener(this);
        mAddress.setOnClickListener(this);
        mWebSite.setOnClickListener(this);
        mPlace = getIntent().getParcelableExtra(DETAIL_PLACE);
        setData();
    }

    /**
     * This function assign data to all views of the detail
     */
    public void setData() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mPlace.getmNamePlace());
            mImageView.setImageDrawable(getResources().getDrawable(mPlace.getmImages()));
            mDescription.setText(mPlace.getmDescription());
            if (!mPlace.getmPhone().equals("--")) {
                mPhone.setText(mPlace.getmPhone());
            } else {
                mPhone.setVisibility(View.GONE);
            }
            mAddress.setText(mPlace.getmAddress());
            if (!mPlace.getmWebSite().endsWith("--")) {
                mWebSite.setText(mPlace.getmWebSite());
            } else {
                mWebSite.setVisibility(View.GONE);
            }
        }
    }

    /**
     * This function take one parameter of type ToolBar and is assigns to action bar
     * and actives the icon backHome
     *
     * @param toolbar for assigns to actionBar
     */
    private void setActionBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_keyboard_backspace_white_24dp));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txvPhone:
                openAction("tel:+503" + mPlace.getmPhone(), ACTION_VIEW_PHONE, Intent.ACTION_DIAL);
                break;
            case R.id.txvAddress:
                openAction("geo:" + mPlace.getmLatitude() + "," + mPlace.getmLongitude() + "?q=" + mPlace.getmLatitude() + "," + mPlace.getmLongitude() + "(" + mPlace.getmNamePlace() + ")",
                        ACTION_VIEW_MAP,
                        Intent.ACTION_VIEW);
                break;
            case R.id.txvWebSite:
                startActivity(WebSitePlaceActivity.getIntent(this, mPlace.getmWebSite()));
                break;
        }
    }

    /**
     * This function creates an intent implicit for make two actions depending
     * of parameter type tha may are Intent.ACTION_DIAL and Intent.ACTION_VIEW
     *
     * @param path   for creates uri
     * @param type   data for make the condition
     * @param action type action to open
     */
    public void openAction(String path, int type, String action) {
        Uri gmmIntentUri = Uri.parse(path);
        Intent mapIntent = new Intent(action, gmmIntentUri);
        if (type == ACTION_VIEW_MAP) {
            mapIntent.setPackage("com.google.android.apps.maps");
        }
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}
