package com.applaudostudios.guidetourapp.activities;

import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.applaudostudios.guidetourapp.R;
import com.applaudostudios.guidetourapp.fragments.PlacesFragment;
import com.applaudostudios.guidetourapp.model.Place;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener, View.OnFocusChangeListener {

    private static final String ID_CATEGORY_CURRENT = "ID_CATEGORY_CURRENT";
    private static final String NAME_CATEGORY_CURRENT = "NAME_CATEGORY_CURRENT";
    private static final String COLOR_CATEGORY_CURRENT = "COLOR_CATEGORY_CURRENT";
    private static final String TAG_FRAGMENT_PLACE = "PLACE";
    private DrawerLayout mDwGuide;
    private int mCurrentIdCategory;
    private String mCurrentNameCategory;
    private int mCurrentColor;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    /**
     * This function initializes all the views
     */
    private void initView() {
        setActionBar(((Toolbar) findViewById(R.id.tbGuide)));

        mDwGuide = findViewById(R.id.dwGuide);
        NavigationView nvGuide = findViewById(R.id.navGuide);
        nvGuide.setNavigationItemSelectedListener(this);
        setFragmentAndChangeName();
    }

    /**
     * This function take one parameter of type ToolBar and is assigns to action bar
     * and actives the icon backHome
     *
     * @param toolbar for assigns to actionBar
     */
    private void setActionBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem mSearch = menu.findItem(R.id.searchAction);
        mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search");
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        mSearchView.setOnQueryTextFocusChangeListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                behaveNav();
                mSearchView.clearFocus();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * this function call behaveNav(values..)
     * with values for defect
     */
    private void behaveNav() {
        behaveNav(true, null, "", 0, 0);
    }

    /**
     * this function open and close drawer and set adds or replaces the fragment
     *
     * @param open       boolean for open or close drawer
     * @param item       menu for marker it
     * @param title      of category
     * @param idCategory int id category
     * @param color      int resource
     */
    private void behaveNav(boolean open, MenuItem item, String title, int idCategory, int color) {
        if (open) {
            mDwGuide.openDrawer(Gravity.START);
        } else {
            mDwGuide.closeDrawers();
            item.setCheckable(true);
            setFragmentAndChangeName(title, idCategory, color);
        }
    }

    /**
     * this function call setFragmentAndChangeName(parameters..)
     * with values for defect
     */
    private void setFragmentAndChangeName() {
        setFragmentAndChangeName(getResources().getString(R.string.title_menu_drawer_volcano), 0, getResources().getColor(R.color.volcanoColor));
    }

    /**
     * This function change the color of actionbar save
     * mCurrentIdCategory, mCurrentNameCategory, mCurrentColor
     * and call setOrReplaceFragment() for add or replace fragment
     *
     * @param title      name of category
     * @param idCategory id of category
     * @param color      id of the resource for change actionBar
     */
    private void setFragmentAndChangeName(String title, int idCategory, int color) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
            getSupportActionBar().setTitle(title);
        }
        mCurrentIdCategory = idCategory;
        mCurrentNameCategory = title;
        mCurrentColor = color;
        setOrReplaceFragment(idCategory);
    }

    /**
     * this function adds or replaces one fragment depending of the category
     *
     * @param idCategory send as argument to fragment
     */
    private void setOrReplaceFragment(int idCategory) {

        if (getSupportFragmentManager().getFragments().size() == 0) {
            getSupportFragmentManager().beginTransaction().add(R.id.rlyContainer, PlacesFragment.newInstance(idCategory), TAG_FRAGMENT_PLACE).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.rlyContainer, PlacesFragment.newInstance(idCategory), TAG_FRAGMENT_PLACE).commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mnVolcano:
                behaveNav(false, item, getResources().getString(R.string.title_menu_drawer_volcano), 0, getResources().getColor(R.color.volcanoColor));
                break;
            case R.id.mnForest:
                behaveNav(false, item, getResources().getString(R.string.title_menu_drawer_forest), 1, getResources().getColor(R.color.forestColor));
                break;
            case R.id.mnBeach:
                behaveNav(false, item, getResources().getString(R.string.title_menu_drawer_beach), 2, getResources().getColor(R.color.beachColor));
                break;
            case R.id.mnAquaticPark:
                behaveNav(false, item, getResources().getString(R.string.title_menu_drawer_aquatic_park), 3, getResources().getColor(R.color.aquaticParkColor));
                break;
            case R.id.mnLake:
                behaveNav(false, item, getResources().getString(R.string.title_menu_drawer_lake), 4, getResources().getColor(R.color.lakeColor));
                break;
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        PlacesFragment placesFragment = (PlacesFragment) getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_PLACE);
        placesFragment.adapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        PlacesFragment placesFragment = (PlacesFragment) getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_PLACE);
        placesFragment.adapter.getFilter().filter(newText);
        return false;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ID_CATEGORY_CURRENT, mCurrentIdCategory);
        outState.putInt(COLOR_CATEGORY_CURRENT, mCurrentColor);
        outState.putString(NAME_CATEGORY_CURRENT, mCurrentNameCategory);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setFragmentAndChangeName(savedInstanceState.getString(NAME_CATEGORY_CURRENT),
                savedInstanceState.getInt(ID_CATEGORY_CURRENT),
                savedInstanceState.getInt(COLOR_CATEGORY_CURRENT));
    }

    @Override
    public boolean onClose() {
        PlacesFragment placesFragment = (PlacesFragment) getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_PLACE);
        placesFragment.adapter.restoreListByFilterClose();
        return false;
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.searchAction:
                if (!hasFocus) {
                    mSearchView.setIconified(true);
                }
                break;
        }
    }
}
