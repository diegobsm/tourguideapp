package com.applaudostudios.guidetourapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applaudostudios.guidetourapp.R;
import com.applaudostudios.guidetourapp.activities.DetailActivity;
import com.applaudostudios.guidetourapp.adapters.CategoryPlaceAdapter;
import com.applaudostudios.guidetourapp.interfaces.OnItemClickPlace;
import com.applaudostudios.guidetourapp.model.Category;
import com.applaudostudios.guidetourapp.model.Place;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesFragment extends Fragment implements OnItemClickPlace {

    private View mView;
    public CategoryPlaceAdapter adapter;
    private String mName;
    private int mIdCategory;
    private static final String EXT_ID_CATEGORY = "EXT_ID_CATEGORY";


    /**
     * This function creates a instance from PlaceFragment and sets the Arguments with the idCategory
     * for load the list correct
     *
     * @param idCategory with the idCategory
     * @return a instance from DetailFragment
     */
    public static PlacesFragment newInstance(int idCategory) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXT_ID_CATEGORY, idCategory);
        PlacesFragment placesFragment = new PlacesFragment();
        placesFragment.setArguments(bundle);
        return placesFragment;
    }

    public PlacesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getArguments() != null) {
            mName = getArguments().getString("nameCategory");
            mIdCategory = getArguments().getInt(EXT_ID_CATEGORY);
        }
        mView = inflater.inflate(R.layout.fragment_places, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }


    /**
     * This function initializes all the views
     */
    public void initView() {
        RecyclerView rcvPlace = mView.findViewById(R.id.rcvPlaces);
        if (getActivity() != null) {
            rcvPlace.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new CategoryPlaceAdapter(this);
            rcvPlace.setAdapter(adapter);
            adapter.setmPlaces(Category.getCategory(mName, mIdCategory, getActivity()).getmPlaces());
        }
    }

    @Override
    public void onClick(Place place) {
        if (getContext() != null) {
            startActivity(DetailActivity.getInstance(getContext(), place));
        }
    }
}
